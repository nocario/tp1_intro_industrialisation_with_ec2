from flask import Flask

app = Flask(__name__)

import boto3


def insert_user_into_dynamo(user):
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("GameScores")
    table.put_item(Item={"id": user["id"], "name": user["name"], "email": user["email"]})



@app.route("/")
def hello():
    return "Hello World!"

@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"



if __name__ == "__main__":
    app.run(host="0.0.0.0")
